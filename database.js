const {Client} = require('pg')

const client = new Client({
  host: 'localhost', // server name or IP address;
  port: 5432,
  database: 'college',
  user: 'postgres',
  password: '123'
});

client.connect();

client.query(`Select * from enrollments`, (err, res) => {
  console.log(res);
  client.end;
})

// DROP TABLE IF EXISTS enrollments;
//
// CREATE TABLE enrollments (
//   id serial,
//   enrollment varchar(255),
//   name varchar(255) default NULL,
//   email varchar(255) default NULL,
//   cpf varchar(15) default NULL,
//   PRIMARY KEY (id)
// );
//
// INSERT INTO enrollments (enrollment,name,email,cpf)
// VALUES
// (714,'Hamilton Patrick','sem.mollis.dui@natoquepenatibus.co.uk',69065511),
//   (377,'Venus Valencia','aliquam.eros.turpis@necurnasuscipit.edu',364997264),
//   (633,'Dennis Fisher','fusce.feugiat@auctorvelitaliquam.co.uk',291373025),
//   (830,'Arden Barron','fusce.feugiat@ornareliberoat.ca',1630571),
//   (478,'Nichole Gallagher','orci.phasellus.dapibus@lectus.net',21400610);