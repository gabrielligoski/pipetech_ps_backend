var express = require("express");
var app = express();

app.use(express.json());

app.listen(3000, () => {
  console.log("Server running on port 3000");
});

const {Client} = require('pg')

const client = new Client({
  host: 'localhost', // server name or IP address;
  port: 5432,
  database: 'college',
  user: 'postgres',
  password: '123'
});

client.connect();

async function getAll() {
  const sql = 'Select * from enrollments';
  return await client.query(sql);
}

async function getByID(req) {
  const sql = `SELECT * FROM enrollments WHERE enrollment = '${req.params.enrollment}'`;
  return await client.query(sql);
}

async function post(req) {
  const sql = `INSERT INTO enrollments (enrollment, name, email, cpf) VALUES (${req.body.enrollment}, '${req.body.name}', '${req.body.email}', ${req.body.cpf})`;
  return await client.query(sql);
}

async function put(req) {
  const sql = `UPDATE enrollments set name = '${req.body.name}', email = '${req.body.email}' WHERE enrollment = '${req.params.enrollment}'`;
  return await client.query(sql);
}

async function deleteByID(req) {
  const sql = `DELETE FROM enrollments WHERE enrollment = '${req.params.enrollment}'`;
  return await client.query(sql);
}

// getAll
app.get("/api/college",  async (req, res, next) => {
  const query = await getAll();
  res.json(query.rows);

  if(query.rowCount>0)
    res.sendStatus(200)
  else
    res.sendStatus(404)

});

// getByID
app.get("/api/college/:enrollment",  async (req, res, next) => {
  const query = await getByID(req);
  res.json(query.rows);
});

// post
app.post("/api/college",  async (req, res, next) => {
  const query = await post(req);

  if(query.rowCount>0)
    res.sendStatus(200)
  else
    res.sendStatus(404)
});

// put
app.put("/api/college/:enrollment",  async (req, res, next) => {
  const query = await put(req);

  if(query.rowCount>0)
    res.sendStatus(200)
  else
    res.sendStatus(404)
});

// delete
app.delete("/api/college/:enrollment",  async (req, res, next) => {
  console.log(req)
  const query = await deleteByID(req);
  if(query.rowCount>0)
    res.sendStatus(200)
  else
    res.sendStatus(404)
});

